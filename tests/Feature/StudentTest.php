<?php

namespace Tests\Feature;

use App\Models\Admin;
use App\Models\School;
use App\Models\Student;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;//, RefreshDatabase;

    /**
     * Default preparation for each test
     *
     */
    public function setUp():void
    {
        /** @var Authenticatable $user*/
        parent::setUp();
        $user = Admin::factory()->create();
        $this->actingAs($user, 'admin');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_access_students()
    {
        $response = $this->get('/admin/students');
        $response->assertSee("الطلاب");
    }

    public function test_auth_user_can_create_student()
    {
        $response = $this->get('/admin/students/create');
        $response->assertSee(['الاسم','الحالة','المدرسة']);
    }

    public function test_data_persist_to_storage_after_submitting()
    {
        Student::factory(5)->create();
        $this->assertCount(5, Student::all());
    }

    public function test_relation_with_school()
    {
        $school = School::factory()->create();
        $student = Student::factory()->for($school)->create();

        $this->assertEquals($student->school_id, $school->id);
    }

    public function test_that_delete_persists_to_storage()
    {
        $student = Student::factory()->create();

        $student->delete();

        $this->assertSoftDeleted($student);
    }
}
