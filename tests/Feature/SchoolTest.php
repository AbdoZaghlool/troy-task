<?php

namespace Tests\Feature;

use App\Models\School;
use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SchoolTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * just fixed things before all tests
     *
     * @return School $school
     */
    public function setUpThings():School
    {
        $school = School::factory()->create();

        $studentOne = Student::factory()->create(['school_id'=>null]);
        $studentTwo = Student::factory()->create(['school_id'=>null]);

        $school->addStudent($studentOne);
        $school->addStudent($studentTwo);

        return $school;
    }

    public function test_school_can_accept_students()
    {
        $school = $this->setUpThings();
        $this->assertEquals(2, $school->studentsCount());
    }

    public function test_school_can_remove_student()
    {
        $school = $this->setUpThings();

        $studentThree = Student::factory()->create(['school_id'=>null]);
        $school->addStudent($studentThree);

        $school->removeStudent($studentThree);

        $this->assertEquals(2, $school->studentsCount());
    }
}
