    - create two crud schools and students.
    -school columns (name, status)
    -students columns (name, status, order)
    -two table with mirage, factory, seed, soft_delete
    -on create new student add student number by school

    ex
    student table

    id,      name     ,  school_id , order

    1,  student_name_1,     1,        1

    1,  student_name_2,     1,        2

    1,  student_name_2,     2,        1
