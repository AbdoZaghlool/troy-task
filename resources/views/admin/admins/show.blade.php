@extends('admin.layouts.master')

@section('title', 'الموظفين')

@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">عرض بيانات الموظف: {{ $admin->name }}</h3>

        {!! Form::model($admin) !!}
        @include('admin.admins._form',['readonly' => true])

        <div class="form-group">
            <a href="{{ route('admins.index') }}" class="btn btn-info">الرجوع</a>
        </div>
    </div>

@endsection
