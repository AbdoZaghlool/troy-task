<div class="row_form">
    <div class="form-group mb-3">
        {!! Form::label('name', 'الاسم', ['class' => 'form-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
        <x-forms.errors name="name" />
    </div>

    <div class="form-group mb-3">
        {!! Form::label('email', 'البريد الالكتروني', ['class' => 'form-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
        <x-forms.errors name="email" />
    </div>

    <div class="form-group mb-3">
        {!! Form::label('phone', 'الهاتف', ['class' => 'form-label']) !!}
        {!! Form::text('phone', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
        <x-forms.errors name="phone" />
    </div>

    @if (!$readonly)
        <div class="form-group mb-3">
            {!! Form::label('password', 'الرقم السري', ['class' => 'form-label']) !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
            <x-forms.errors name="password" />
        </div>

        <div class="form-group mb-3">
            {!! Form::label('password_confirmation', ' تاكيد الرقم السري ', ['class' => 'form-label']) !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            <x-forms.errors name="password_confirmation" />
        </div>
    @endif


    <div class="form-group mb-3">
        {!! Form::label('active', 'تفعيل', ['class' => 'form-label']) !!}
        {!! Form::select('active', active(), null, ['class' => 'form-control', 'disabled' => $readonly, 'placeholder' => 'اختر قيمة']) !!}
        <x-forms.errors name="active" />
    </div>

    @if (!$readonly)
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"> حفظ</button>
        </div>
    @endif
</div>
