@extends('admin.layouts.master')

@section('title', 'المشرفين')

@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">اضافة مدير جديد</h3>

        {!! Form::open(['route' => 'admins.store']) !!}
        @include('admin.admins._form',['readonly'=>false, 'admin' => null])
        {!! Form::close() !!}

    </div>
@endsection
