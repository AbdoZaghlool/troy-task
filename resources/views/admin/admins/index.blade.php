@extends('admin.layouts.master')

@section('title', 'المشرفين')

@section('content')

    <div class="card-box">
        <h4 class="mt-0 header-title">عرض المشرفين</h4>
        <p class="text-muted font-14 mb-3">
            <a href="{{ route('admins.create') }}" class="btn btn-info">اضافة جديد</a>
        </p>

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" aria-controls="datatable-buttons" aria-sort="ascending">ID</th>
                                <th class="sorting" aria-controls="datatable-buttons">الاسم</th>
                                <th class="sorting" aria-controls="datatable-buttons">البريد الالكتروني</th>
                                <th class="sorting" aria-controls="datatable-buttons">الهاتف</th>
                                <th class="sorting" aria-controls="datatable-buttons">التفعيل</th>
                                <th class="sorting" aria-controls="datatable-buttons">العمليات</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($admins as $admin)
                                <tr role="row" class="odd" id="row-{{ $admin->id }}">
                                    <td class="sorting_1">{{ $admin->id }}</td>
                                    <td>{{ $admin->name }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>{{ $admin->phone }}</td>
                                    <td>
                                        @if ($admin->active)
                                            <h6 class="btn btn-success btn-circule"> مفعل</h6>
                                        @else
                                            <h6 class="btn btn-danger"> غير مفعل</h6>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a href="{{ route('admins.show', $admin) }}" class="btn btn-success">استعراض</a> --}}
                                        <a href="{{ route('admins.edit', $admin) }}" class="btn btn-info">تعديل</a>
                                        @if ($admin->id !== auth('admin')->user()->id)
                                            <h6 class="btn btn-danger" data-name="{{ $admin->name }}"
                                                data-url="{{ route('admins.destroy', $admin) }}"
                                                onclick="delete_form(this)">
                                                حذف
                                            </h6>
                                        @endif
                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>

                    <form style="display: none;" id="delete_form" method="post">
                        @csrf @method('DELETE')
                    </form>

                </div>
            </div>

        </div>
    </div>


@endsection

@include('admin.layouts.partials.datatables')
