@extends('admin.layouts.master')

@section('title', 'المشرفين')


@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">تعديل الموظف: {{ $admin->name }}</h3>

        {!! Form::model($admin, ['route' => ['admins.update', $admin], 'method' => 'PUT']) !!}
        @include('admin.admins._form',['readonly'=>false])
        {!! Form::close() !!}

    </div>
@endsection
