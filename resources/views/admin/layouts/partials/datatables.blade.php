@push('styles')
    <link href="{{ asset('light/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('light/libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('light/libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('light/libs/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('light/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('light/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('light/libs/switchery/switchery.min.css') }}" rel="stylesheet" />
@endpush



@push('scripts')
    <script src="{{ asset('light/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('light/libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('light/libs/switchery/switchery.min.js') }}"></script>

    <script src="{{ asset('light/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('light/libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('light/js/pages/datatables.init.js') }}"></script>

    <script>
        function delete_form(element) {
            var name = $(element).data('name');
            var swalTitle = 'حذف: ' + name;
            var swalText = 'انت علي وشك حذف ' + name + 'هل تود المتابعة؟';
            swal({
                title: swalTitle,
                text: swalText,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "متابعة",
                cancelButtonText: "الغاء"
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    $('#delete_form').attr('action', $(element).data('url')).submit();
                }
            });
        }
    </script>
@endpush
