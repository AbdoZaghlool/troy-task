<div class="left-side-menu">

    <div class="slimScrollDiv active" style="position: relative; overflow: hidden; width: auto; height: auto;">
        <div class="slimscroll-menu in" style="overflow: auto; width: auto; height: auto;">

            <!--- Sidemenu -->
            <div id="sidebar-menu" class="active">

                <ul class=" in" id="side-menu">

                    <li class="menu-title">عناصر اللوحة</li>

                    <li class="{{ strpos(url()->current(), '/home') !== false ? 'active' : '' }}">
                        <a href="{{ route('admin.home') }}">
                            <i class="fas fa-home text-dark"></i>
                            <span> الرئيسية </span>
                        </a>
                    </li>

                    <li class="{{ strpos(url()->current(), '/admins') !== false ? 'active' : '' }}">
                        <a href="{{ route('admins.index') }}">
                            <i class="mdi mdi-view-dashboard"></i>
                            <span> الادارة </span>
                        </a>
                    </li>

                    <li class="{{ strpos(url()->current(), '/schools') !== false ? 'active' : '' }}">
                        <a href="{{ route('schools.index') }}">
                            <i class="mdi mdi-view-dashboard"></i>
                            <span> المدارس </span>
                        </a>
                    </li>

                    <li class="{{ strpos(url()->current(), '/students') !== false ? 'active' : '' }}">
                        <a href="{{ route('students.index') }}">
                            <i class="mdi mdi-view-dashboard"></i>
                            <span> الطلاب </span>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <div class="slimScrollBar"
            style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: -116.656px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 51.6253px;">
        </div>
        <div class="slimScrollRail"
            style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
        </div>
    </div>
    <!-- Sidebar -left -->

</div>
