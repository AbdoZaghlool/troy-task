<!-- Vendor js -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
@toastr_js
@toastr_render
<script src="{{asset('light/js/vendor.min.js')}}"></script>
<script src="{{asset('light/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('light/js/pages/sweet-alerts.init.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- App js -->
{{-- <script src="{{asset('light/js/app.min.js')}}"></script> --}}
@stack('scripts')
