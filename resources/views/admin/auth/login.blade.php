@extends('admin.layouts.auth')


@section('content')
<div class="col-md-6" dir="rtl">
    <div class="card-box">
        <h4 class="mt-0 mb-3 header-title">تسجيل الدخول</h4>

        <form role="form">
            <div class="form-group">
                <label for="exampleInputEmail1">البريد الالكتروني</label>
                <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">
                    @error('email')
                    {{$message}}
                    @enderror
                </small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">الرقم السري</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                <small id="passHelp" class="form-text text-muted">
                    @error('password')
                    {{$message}}
                    @enderror
                </small>
            </div>

            <button type="submit" class="btn btn-primary">تسجيل الدخول</button>
        </form>
    </div>
</div>

@endsection
