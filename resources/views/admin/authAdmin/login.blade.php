@extends('admin.authAdmin.master')

@section('title', 'تسجيل الدخول')

@section('content')
{{--  @if (session('An_error_occurred'))
<div class="alert alert-success">
    {{ session('An_error_occurred') }}
</div>
@endif
@if (session('warning_login'))
<div class="alert alert-danger">
    {{ session('warning_login') }}
</div>
@endif --}}


<div class="col-md-8 col-lg-6 col-xl-5">
    <div class="text-center">
        <a href="{{url()->current()}}">
            <span><img src="{{asset('light/assets/images/logo-light.png')}}" alt="" height="22"></span>
        </a>
        <p class="text-muted mt-2 mb-4">قم بكتابة بيانات الدخول</p>
    </div>
    <div class="card">

        <div class="card-body p-4">

            <div class="text-center mb-4">
                <h4 class="text-uppercase mt-0">تسجيل الدخول</h4>
            </div>

            <form action="{{route('admin.login.submit')}}" method="POST">@csrf

                <div class="form-group mb-3">
                    <label for="emailaddress">البريد الالكتروني</label>
                    <input class="form-control {{$errors->has('email') ? 'parsley-error':''}}" name="email" type="email"
                        parsley-trigger="change" id="emailaddress" placeholder="your email" value="{{old('email')}}">
                    @error('email')
                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                        <li class="parsley-required">{{$message}}</li>
                    </ul>
                    @enderror
                </div>

                <div class="form-group mb-3">
                    <label for="password">الرقم السري</label>
                    <input class="form-control {{$errors->has('password') ? 'parsley-error':''}}" name="password"
                        type="password" id="password" placeholder="your password">
                    @error('password')
                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                        <li class="parsley-required">{{$message}}</li>
                    </ul>
                    @enderror
                </div>

                <div class="form-group mb-0 text-center">
                    <button class="btn btn-primary btn-block" type="submit"> تسجيل الدخول </button>
                </div>

            </form>

        </div> <!-- end card-body -->
    </div>
    <!-- end card -->

    {{-- <div class="row mt-3">
        <div class="col-12 text-center">
            <p> <a href="{{route('admin.password.request')}}" class="text-muted ml-1"><i
                        class="fa fa-lock mr-1"></i>نسيت الرقم
                    السري؟</a></p>
        </div> <!-- end col -->
    </div> --}}
    <!-- end row -->

</div>


@endsection
