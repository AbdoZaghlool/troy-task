@extends('admin.layouts.master')

@section('title', 'المدارس')

@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">عرض بيانات المدرسة: {{ $school->name }}</h3>

        {!! Form::model($school) !!}
        @include('admin.schools._form',['readonly' => true])

        <div class="form-group">
            <a href="{{ route('schools.index') }}" class="btn btn-info">الرجوع</a>
        </div>
    </div>

@endsection
