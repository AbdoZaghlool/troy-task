@extends('admin.layouts.master')

@section('title', 'المدارس')

@section('content')

    <div class="card-box">
        <h4 class="mt-0 header-title">عرض المدارس</h4>
        <p class="text-muted font-14 mb-3">
            <a href="{{ route('schools.create') }}" class="btn btn-info">اضافة جديد</a>
        </p>

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" aria-controls="datatable-buttons" aria-sort="ascending">ID</th>
                                <th class="sorting" aria-controls="datatable-buttons">الاسم</th>
                                <th class="sorting" aria-controls="datatable-buttons">التفعيل</th>
                                <th class="sorting" aria-controls="datatable-buttons">العمليات</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($schools as $school)
                                <tr role="row" class="odd" id="row-{{ $school->id }}">
                                    <td class="sorting_1">{{ $school->id }}</td>
                                    <td>{{ $school->name }}</td>
                                    <td>
                                        @if ($school->status)
                                            <h6 class="btn btn-success btn-circule"> مفعل</h6>
                                        @else
                                            <h6 class="btn btn-danger"> غير مفعل</h6>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('schools.show', $school) }}" class="btn btn-success">استعراض</a>
                                        <a href="{{ route('schools.edit', $school) }}" class="btn btn-info">تعديل</a>
                                        <h6 class="btn btn-danger" data-name="{{ $school->name }}"
                                            data-url="{{ route('schools.destroy', $school) }}" onclick="delete_form(this)">
                                            حذف
                                        </h6>
                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>

                    <form style="display: none;" id="delete_form" method="post">
                        @csrf @method('DELETE')
                    </form>

                </div>
            </div>

        </div>
    </div>


@endsection

@include('admin.layouts.partials.datatables')
