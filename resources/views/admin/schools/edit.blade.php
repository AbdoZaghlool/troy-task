@extends('admin.layouts.master')

@section('title', 'المدارس')


@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">تعديل مدرسة: {{ $school->name }}</h3>

        {!! Form::model($school, ['route' => ['schools.update', $school], 'method' => 'PUT']) !!}
        @include('admin.schools._form',['readonly'=>false])
        {!! Form::close() !!}

    </div>
@endsection
