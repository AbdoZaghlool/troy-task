@extends('admin.layouts.master')

@section('title', 'الطلاب')

@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">عرض بيانات الطالب: {{ $student->name }}</h3>

        {!! Form::model($student) !!}
        @include('admin.students._form',['readonly' => true])

        <div class="form-group">
            <a href="{{ route('students.index') }}" class="btn btn-info">الرجوع</a>
        </div>
    </div>

@endsection
