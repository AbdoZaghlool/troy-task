@extends('admin.layouts.master')

@section('title', 'الطلاب')

@section('content')

    <div class="card-box">
        <h4 class="mt-0 header-title">عرض الطلاب</h4>
        <p class="text-muted font-14 mb-3">
            <a href="{{ route('students.create') }}" class="btn btn-info">اضافة جديد</a>
        </p>

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" aria-controls="datatable-buttons" aria-sort="ascending">ID</th>
                                <th class="sorting" aria-controls="datatable-buttons">الاسم</th>
                                <th class="sorting" aria-controls="datatable-buttons">المدرسة</th>
                                <th class="sorting" aria-controls="datatable-buttons">الترتيب</th>
                                <th class="sorting" aria-controls="datatable-buttons">التفعيل</th>
                                <th class="sorting" aria-controls="datatable-buttons">العمليات</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($students as $student)
                                <tr role="row" class="odd" id="row-{{ $student->id }}">
                                    <td class="sorting_1">{{ $student->id }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->school->name }}</td>
                                    <td>
                                        @if (!$student->order)
                                            <a class="btn btn-bordred-warning"
                                                data-url="{{ route('students.setOrder', $student->id) }}"
                                                onclick="update_form(this)" data-id="{{ $student->id }}">
                                                حفظ الرقم
                                            </a>
                                        @else
                                            {{ $student->order }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($student->status)
                                            <h6 class="btn btn-success btn-circule"> مفعل</h6>
                                        @else
                                            <h6 class="btn btn-danger"> غير مفعل</h6>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a href="{{ route('students.show', $student) }}" class="btn btn-success">استعراض</a> --}}
                                        <a href="{{ route('students.edit', $student) }}" class="btn btn-info">تعديل</a>

                                        <h6 class="btn btn-danger" data-name="{{ $student->name }}"
                                            data-url="{{ route('students.destroy', $student) }}"
                                            onclick="delete_form(this)">
                                            حذف
                                        </h6>
                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>

                    <form style="display: none;" id="delete_form" method="post">
                        @csrf @method('DELETE')
                    </form>

                    <form style="display: none;" id="update_form" method="post">
                        @csrf
                        <input id="order_input" type="hidden" name="order">
                    </form>

                </div>
            </div>

        </div>
    </div>


@endsection



@push('scripts')

    <script>
        async function update_form(element) {
            const {
                value: order
            } = await swal.fire({
                title: 'الترتيب',
                input: 'text',
                inputPlaceholder: 'ادخل ترتيب الطالب'
            })

            if (order) {
                $("#order_input").val(order);
                $("#update_form").attr('action', $(element).data('url')).submit();
            }
        }
    </script>
@endpush


@include('admin.layouts.partials.datatables')
