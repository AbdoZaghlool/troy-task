<div class="row_form">
    <div class="form-group mb-3">
        {!! Form::label('name', 'الاسم', ['class' => 'form-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
        <x-forms.errors name="name" />
    </div>

    <div class="form-group mb-3">
        {!! Form::label('_status', 'الحالة', ['class' => 'form-label']) !!}
        {!! Form::select('status', active(), null, ['class' => 'form-control', 'id' => '_status', 'disabled' => $readonly, 'placeholder' => 'اختر قيمة']) !!}
        <x-forms.errors name="status" />
    </div>

    <div class="form-group mb-3">
        {!! Form::label('school_id', 'المدرسة', ['class' => 'form-label']) !!}
        {!! Form::select('school_id', schools(), null, ['class' => 'form-control', 'disabled' => $readonly, 'placeholder' => 'اختر قيمة']) !!}
        <x-forms.errors name="school_id" />
    </div>

    @if (!$readonly)
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"> حفظ</button>
        </div>
    @endif
</div>
