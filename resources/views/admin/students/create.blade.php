@extends('admin.layouts.master')

@section('title', 'الطلاب')

@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">اضافة جديد</h3>

        {!! Form::open(['route' => 'students.store']) !!}
        @include('admin.students._form',['readonly'=>false])
        {!! Form::close() !!}

    </div>
@endsection
