@extends('admin.layouts.master')

@section('title', 'الطلاب')


@section('content')
    <div class="card-box">
        <h3 class="form-title font-green">تعديل الطالب: {{ $student->name }}</h3>

        {!! Form::model($student, ['route' => ['students.update', $student], 'method' => 'PUT']) !!}
        @include('admin.students._form',['readonly'=>false])
        {!! Form::close() !!}

    </div>
@endsection
