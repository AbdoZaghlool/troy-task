<div>
    @error($name)
    <ul class="parsley-errors-list filled" id="parsley-id-5">
        <li class="parsley-required">{{$message}}</li>
    </ul>
    @enderror
</div>