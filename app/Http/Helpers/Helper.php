<?php

function active()
{
    return [
        '0' => 'غير مفعل',
        '1' => 'مفعل',
    ];
}

function schools()
{
    return \App\Models\School::where('status', 1)->pluck('name', 'id');
}
