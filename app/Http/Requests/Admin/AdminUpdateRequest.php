<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'email'    => 'required|email|unique:admins,email,'.$this->route('admin')->id,
            'phone'    => 'required|min:10|unique:admins,phone,'.$this->route('admin')->id,
            'active'   => 'required|in:0,1',
            'password' => 'nullable|min:6|confirmed',
        ];
    }
}
