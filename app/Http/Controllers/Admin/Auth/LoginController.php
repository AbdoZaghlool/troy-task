<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $redirect = '/admin/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.authAdmin.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);
        $credential =[
                'email'=>$request->email,
                'password'=>$request->password
            ];
        if (Auth::guard('admin')->attempt($credential)) {
            return redirect()->intended(route('admin.home'));
        }
        toastr()->error(trans('auth.failed'));
        return redirect()->back()->withInput($request->only('email'))->with('warning_login', 'these cerdentials are false');
    }

    public function redirectPath()
    {
        return '/admin/home';
    }

    /**
     * log user out from system
     *
     * @return void
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}