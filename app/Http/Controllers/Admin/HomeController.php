<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
/**
     * update admin information to storage
     *
     * @param Request $request
     * @return void
     */
    public function updateProfile(Request $request)
    {
        $this->validate($request, $this->rules());
        $request->user()->update($request->only('name', 'email', 'phone', 'password'));
        toastr()->success(__('dashboard.updated'));
        return back();
    }
    /**
     * validation rules for updateing admin info
     *
     * @return array rules
     */
    protected function rules()
    {
        return [
            'name'     => 'required',
            'email'    => 'required|email|unique:admins,email,' . auth('admin')->user()->id,
            'phone'    => 'required|min:10|unique:admins,phone,' . auth('admin')->user()->id,
            'password' => 'nullable|min:6|confirmed',
        ];
    }
}
