<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\School\SchoolRequest;
use App\Models\School;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.schools.index', ['schools' => School::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  choolRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        School::create($request->validated());
        toastSuccess('created successfully');
        return redirect(route('schools.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        return view('admin.schools.show', ['school' =>$school]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        return view('admin.schools.edit', ['school' =>$school]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SchoolRequest  $request
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolRequest $request, School $school)
    {
        $school->update($request->validated());
        toastSuccess('updated successfully');
        return redirect(route('schools.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        $school->delete();
        toastSuccess('deleted successfully');
        return back();
    }
}
