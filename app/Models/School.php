<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','status','deleted_at'];

    /**
     * Get all of the students for the School
     *
     * @return HasMany
     */
    public function students(): HasMany
    {
        return $this->hasMany(Student::class);
    }

    public function addStudent($student)
    {
        $this->students()->save($student);
    }

    public function studentsCount()
    {
        return $this->students()->count();
    }

    public function removeStudent(Student $student)
    {
        $student->update(['school_id'=>null]);
    }
}
