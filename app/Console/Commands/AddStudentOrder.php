<?php

namespace App\Console\Commands;

use App\Models\Student;
use Illuminate\Console\Command;

class AddStudentOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'student:add-order {student} {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to set student order to storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Student::find($this->argument('student'))->update(['order'=>$this->argument('order')]);

        // if we need to run it automatically then we need to run task schedule then loop throw all students to assign each of them order
    }
}
