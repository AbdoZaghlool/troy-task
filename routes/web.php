<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome');




Route::prefix('admin')->group(function () {
    Route::get('login', [Auth\LoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('login', [Auth\LoginController::class, 'login'])->name('admin.login.submit');
    Route::post('logout', [Auth\LoginController::class, 'logout'])->name('admin.logout');

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::view('/home', 'admin.home')->name('admin.home');
        Route::view('/my-profile', 'admin.profile')->name('admin.profile');
        Route::put('/my-profile', [HomeController::class, 'updateProfile'])->name('admin.updateProfile');

        Route::resources([
            'admins' => AdminController::class,
            'schools' => SchoolController::class,
            'students' => StudentController::class,
        ]);

        Route::post('students/{id}/set-order', [StudentController::class,'setOrder'])->name('students.setOrder');
    });
});
